$(document).ready( function () {
    $('#table_id').DataTable();
} );

const firstName = $("#firstName")
const lastName = $("#lastName")
const birthday = $("#birthday")
const status = $("#status")
const form = $(".formValid")
const quizBox = $('.quizBox')

const startBtn = $(".startBtn")

let quizCount = 0
let score = 0
let questionNumber = 0


// Basic form input change validation

firstName.change(function() {
    // On change, is the input value is NOT empty:
    if (firstName.val() != "") {
        // Add the valid class
        firstName.addClass("is-valid").removeClass("is-invalid").next().hide()
    } else { // Or else, remove it when emptied.
        firstName.removeClass("is-valid")
    }
})

lastName.change(function() {
    // On change, is the input value is NOT empty:
    if (lastName.val() != "") {
        // Add the valid class
        lastName.addClass("is-valid").removeClass("is-invalid").next().hide()
    } else { // Or else, remove it when emptied.
        lastName.removeClass("is-valid")
    }
})

status.change(function() {
    // On change, is the input value is NOT empty:
    if (status.val() != "") {
        // Add the valid class
        status.addClass("is-valid").removeClass("is-invalid").next().hide()
    } else { // Or else, remove it when emptied.
        status.removeClass("is-valid")
    }
})



// Submit validation function

startBtn.on("click", (e)=> { // On click start button

    // Lets the console know if the inputs are either valid (true) or not (false)
    let fnValid = false
    let lnValid = false
    let statusValid = false

    // First Name input check
    // If input is an empty string:
    if (firstName.val() == "" || firstName.val() == null) {
        // Remove valid class and show invalid response:
        firstName.removeClass("is-valid").addClass("is-invalid").next().show()
        $(".registerAlert").show()

        fnValid = false // Input is invalid
        
    } else {fnValid = true} // If not, the input is valid.

    // Last Name input check
    // If input is an empty string:
    if (lastName.val() == "" || lastName.val() == null) {
        // Remove valid class and show invalid response:
        lastName.removeClass("is-valid").addClass("is-invalid").next().show()
        $(".registerAlert").show()

        lnValid = false // Input is invalid

    } else {lnValid = true} // If not, the input is valid.

    // Birthday date input check
    
    // Current status select check
    // If selection's value is empty:
    if (status.val() == "" || status.val() == null) {
        // Remove valid class and show invalid response:
        status.removeClass("is-valid").addClass("is-invalid").next().show()
        $(".registerAlert").show()
        
        statusValid = false // Selection is invalid

    } else {statusValid = true} // If not, the selection is valid.


    // If one or more input is invalid
    if (fnValid == false || lnValid == false || statusValid == false) {
        // prevent the form from submitting and starting the quiz
        e.preventDefault()

    } else { // Otherwise, submit and start the quiz
        $(".registerAlert").hide()
        $(".myForm").fadeOut({queue: false, duration: 'slow'})
        $(".myForm").animate({bottom: '300px'}, 'slow')
        quizBox
            .delay(800)
            .show()
            .animate({queue: false, top: "0px", opacity: 1}, 'slow')
}
})


// Load JSON quiz data into a function call
$.getJSON("/dist/js/quiz_data.json", function(quizData){
    // Confirm that the JSON file has succesfully been loaded into the DOM.
    console.log("- JSON data has successfully been imported!")

    // Create the quiz's questions after the page has been loaded.

    $(".question1")
    .addClass("activeQ")
    .append('<h2> Question 1: <br><br> ' + quizData[0].question + '</h2>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3 good">'+quizData[0].réponses[0]+'</button>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3">'+quizData[0].réponses[1]+'</button>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3">'+quizData[0].réponses[2]+'</button>')

    $(".question2")
    .append('<h2> Question 2: <br><br> ' + quizData[1].question + '</h2>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3">'+quizData[1].réponses[0]+'</button>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3 good">'+quizData[1].réponses[1]+'</button>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3">'+quizData[1].réponses[2]+'</button>')

    $(".question3")
    .append('<h2> Question 3: <br><br> ' + quizData[2].question + '</h2>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3">'+quizData[2].réponses[0]+'</button>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3">'+quizData[2].réponses[1]+'</button>')
    .append('<button type="button" class="btn btn-answer mt-3 mb-3 good">'+quizData[2].réponses[2]+'</button>')

    // Clicking on answer triggers next question
    $(".btn-answer").on("click", (e)=> {

        var clicked = e.target
        var currentQuestion = $(".question.activeQ")
        $(".question").removeClass("activeQ")
        currentQuestion.next().addClass("activeQ")
        quizCount++

        // If the answer is right, add 1 to score
        if(clicked.classList.contains("good")){
            score ++
            questionNumber ++

            // Append the question result (right answer)
            $("#resultsTable").append(
                "<tr>"+
                    "<td>Question " + questionNumber + "</td>"+
                    "<td><i class='bi bi-check-square goodAnswer'></i></td>"
                )
        } else {
            questionNumber ++
            // Append the question result (wrong answer)
            $("#resultsTable").append(
                "<tr>"+
                    "<td>Question " + questionNumber + "</td>"+
                    "<td><i class='bi bi-x-square badAnswer'></i></td>"
                )
        }
        // If all question are answered, show modal
        if (quizCount == 3) {
            $("#scoreModal").modal("toggle");
            $("#modalName").append("<strong>Name: </strong>" + $("#firstName").val() + " " + $("#lastName").val())
            $("#modalStatus").append("<strong>Status: </strong>" + $("#status").val())
            $("#modalScore").append("<strong>Score: " + score + " / 3</strong>")
            
            //Create accordion with questions and answers
            $("#accordion").append(

                "<h3>"+quizData[0].question+"</h3>"+
                "<div>"+
                    "<ul>"+
                        "<li class='goodAnswer'>"+quizData[0].réponses[0]+"</li>"+
                        "<li class='badAnswer'>"+quizData[0].réponses[1]+"</li>"+
                        "<li class='badAnswer'>"+quizData[0].réponses[2]+"</li>"+
                    "</ul>"+
                "</div>"+

                "<h3>"+quizData[1].question+"</h3>"+
                "<div>"+
                    "<ul>"+
                        "<li class='badAnswer'>"+quizData[1].réponses[0]+"</li>"+
                        "<li class='goodAnswer'>"+quizData[1].réponses[1]+"</li>"+
                        "<li class='badAnswer'>"+quizData[1].réponses[2]+"</li>"+
                    "</ul>"+
                "</div>"+

                "<h3>"+quizData[2].question+"</h3>"+
                "<div>"+
                    "<ul>"+
                        "<li class='badAnswer'>"+quizData[2].réponses[0]+"</li>"+
                        "<li class='badAnswer'>"+quizData[2].réponses[1]+"</li>"+
                        "<li class='goodAnswer'>"+quizData[2].réponses[2]+"</li>"+
                    "</ul>"+
                "</div>"
            )
            $( function () {
                $("#accordion").accordion({heightStyle: "content"});
            } );
        }
    })
    // If score = 3, win
    if (score == 3) {
        $(".modal-title").append("Quiz is over, you win!")
    }   else { // Or else, lose
        $(".modal-title").append("Quiz is over, you lose!")
    }

    // Bug fix where modal wouldn't close on "close" button click.
    $("#modalCloseBtn").on("click", function() {
        $("#scoreModal").modal("toggle")
    })
})

